### What is this repository for? ###

The vCenter Driver is code written to integrate PlexxiCore with the Openstack Policy engine [Congress](https://wiki.openstack.org/wiki/Congress).  It allows congress to create and populate tables with data gained from vCenter for use in future Polices.  

Currently this driver  and this repository  still in the earliest stages of development and only pulling basic information from vCenter. This will be expanded in future updates.

This driver was originally written using pyvmomi and vConnector to connect with vCenter, that version of the driver can be found [here](https://bitbucket.org/ConnerFerguson/vcenter-driver-pvvmomi). This version of the driver removes the need for additional dependencies by using oslo.vmware to connect to vCenter, which is included in openstack installs by default.

### How do I get set up? ###

You will first need congress installed to use the vCenterDriver. If you do not have Congress, you can find it with installation instructions at
https://github.com/stackforge/congress


Once you have congress running:


1) Add the vCenter_Driver.py file to your datasources folder in congress
```
path/to/congress/congress/datasources
```
2)Modify your datasources.conf that your congress server is using to use the vCenter Driver. In a standard devstack install this will be
```
/etc/congress/datasources.conf
```

Add:
```
[vCenter]
module:datasources/vCenter_driver.py
username: vCenter_username
password: vCenter_password
auth_url:  vCenter_URL
max_vms: The maximum number of VMs to be pulled from vCenter
max_hosts: The maximum number of hosts to be pulled from vCenter

```

After the driver has been configured, data from vCenter should be visible through the congress API.

### Testing ###


The files included in the Tests folder are designed to be used on the machine that congress is running on. To  execute tests, place the two files found in the Tests Folder in

```
path/to/congress/congress//tests/datasources
```
When the testing files are in place as well as the main driver you can the command below to test the driver

```
python -m unittest test_vCenter_driver.TestvCenterDriver
```

The output should look similar to
```
.
----------------------------------------------------------------------
Ran 1 tests in 0.008s

OK

```

### Who do I talk to? ###

You can contact me at conner.ferguson1@marist.edu